CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Self Healing URLs is a simple module inspired by [this video from Aaron Francis](https://www.youtube.com/watch?v=a6lnfyES-LA).

The main goal of this module is to heal the URL and redirect it to the correct entity page. It also indicates the browsers that it is a [301 Moved Permanently ](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/301) redirect.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

* Pathauto - https://www.drupal.org/project/pathauto

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
------------

The module currently has no specific configuration.
However, to ensure proper URL healing,
we assume that the URL will always include
a unique ID (entity ID) as the first value in the path.

For instance:

* https://example.com/{node_id}/this-is-my-example-node

In the above example, `{node_id}` represents the unique ID used to identify
and redirect to the correct node.

Therefore, we recommend using [Pathauto](https://www.drupal.org/project/pathauto) to set the URL pattern for the nodes.

MAINTAINERS
-----------

Current maintainers:

* Abhisek Mazumdar - https://www.drupal.org/u/abhisekmazumdar
* Eleo Basili - https://www.drupal.org/u/eleonel
